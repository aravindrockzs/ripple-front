import React from 'react';
import styles from './CampaignBoard.module.css';
import campimg from '../../assets/car4.jpg';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

export default function CampaignBoard(props) {
  const { active, value, content } = props;

  return (
    <div
      className={[
        styles.campcontainer,
        active ? styles.campactive : styles.campinactive,
      ].join(' ')}
    >
      <img src={campimg} alt='' className={styles.campimg} s />

      <div className={styles.camptext}>{content}</div>
      <div
        className={[
          styles.campbar,
          active ? styles.campbaractive : styles.campbarinactive,
        ].join(' ')}
      ></div>
      <div style={{ width: 30, height: 30 }}>
        <CircularProgressbar
          value={value}
          background
          backgroundPadding={6}
          styles={buildStyles({
            backgroundColor: active ? '#000000' : '#ffffff',
            textColor: '#fff',
            pathColor: active ? '#ffffff' : '#000000',
            trailColor: 'transparent',
          })}
        />
      </div>
    </div>
  );
}
