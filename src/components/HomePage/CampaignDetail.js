import React from 'react';
import { useState } from 'react';
import bgimg from '../../assets/car4.jpg';
import share from '../../assets/share.png';
import badge from '../../assets/badge.png';
import clock from '../../assets/clock.png';
import backicon from '../../assets/backion.svg';

import styles from './CampaignDetail.module.css';
import './CampaignDetail.css';
import Accordion from './Accordion';

export default function CampaignDetail(props) {
  let [animate, setAnimate] = useState('active');
  const handleBackbtn = () => {
    animate === 'active' ? setAnimate('passive') : setAnimate('active');

    setTimeout(() => {
      props.handleClick();
    }, 500);
  };

  return (
    <div
      className={`${styles.campDetailContainer} ${animate} `}
      style={{ bottom: '0' }}
    >
      <div onClick={handleBackbtn} className={styles.ExpcardBackbtn}>
        <img
          className={styles.ExpcardBackbtnImg}
          src={backicon}
          alt='back-icon'
        />
      </div>
      <div className={styles.ExpcardContainer}>
        <div className={styles.ExpcardImgContainer}>
          <img src={bgimg} alt='' className={styles.ExpcardImg} />
          <div className={styles.ExpRightItem}>UGC, 1/20 Attempts</div>
          <div>
            <img src={share} alt='' className={styles.ExpshareImg} />
          </div>
        </div>
        <div className={styles.ExpcardContentArea}>
          <div className={styles.ExpcardContentTitle}>Opinion with Badges</div>
          <div className={styles.ExpcardContentDesc}>Opinion with Badges</div>
          <div className={styles.ExpcardContentFlexHolder}>
            <div className={styles.ExpcardContentFlex}>
              <img src={clock} alt='' className={styles.ExpitemImg} />
              <div>Ends in 2 weeks</div>
            </div>
            <div className={styles.ExpcardContentFlex}>
              <img src={badge} alt='' className={styles.ExpitemImg} />
              <div>50% Off On all Products</div>
            </div>
          </div>
          <div className={styles.ExpcardAccordContainer}>
            <Accordion
              title='Rules and Instructions'
              content='What is the one product you have to have with you whenever you’re out, the only beauty product you can’t and won’t skip at all costs?'
            />
            <Accordion
              title='Description'
              content='What is the one product you have to have with you whenever you’re out, the only beauty product you can’t and won’t skip at all costs? '
            />
            <div className={styles.btnGetStarted}>Get Started</div>
          </div>
        </div>
      </div>
    </div>
  );
}
