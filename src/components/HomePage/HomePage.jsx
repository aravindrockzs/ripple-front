import styles from './HomePage.module.css';
import './HomePage.css';
import { useState } from 'react';
import CampaignBoard from './CampaignBoard';
import CampaignDesc from './CampaignDesc';
import ExpCard from './ExpCard';
import Carousel from '../Carousel/Carousel';
import Navbar from '../Navbar/Navbar';
import CampaignDetail from './CampaignDetail';

function HomePage(props) {
  const [showDetail, setDetail] = useState(false);
  let [visibility, setvisibility] = useState('');

  const handleClick = () => {
    setDetail(!showDetail);
    visibility =
      visibility === '' || visibility === 'show'
        ? setvisibility('hide')
        : setvisibility('show');
  };

  return (
    <div className={styles.homeContainer}>
      <div className={styles.homeBg}>
        <div className={`${styles.homeBgInner} ${visibility}`}>
          <Carousel />

          <div className={styles.campaigndescHolder}>
            <CampaignDesc />
          </div>
          <div className={styles.campaignholder}>
            <CampaignBoard active={true} value={65} content={'campaing one'} />
            <CampaignBoard active={false} value={65} content={'campaing two'} />
            <CampaignBoard active={false} value={65} content={'campaing two'} />
          </div>

          <div className={styles.expcardHolder}>
            <ExpCard handleClick={handleClick} />
            <ExpCard handleClick={handleClick} />
          </div>
          <Navbar />
        </div>

        {showDetail && <CampaignDetail handleClick={handleClick} />}
      </div>
    </div>
  );
}

export default HomePage;
