import React from 'react'
import styles from './CampaignDesc.module.css'
import confetti from '../../assets/confetti.png'

export default function CampaignDesc() {
    return (
        <div className={styles.CampaignDescContainer}>
            <img src={confetti} alt=""   />
            <div className={styles.CampaignDesctext}>Win these quizzes and snag 3 badges to compete for the beauty champion trophy.</div>
        </div>
    )
}
