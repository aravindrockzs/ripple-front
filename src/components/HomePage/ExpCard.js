import React from 'react';
import styles from './ExpCard.module.css';

import bgimg from '../../assets/car4.jpg';
import share from '../../assets/share.png';
import badge from '../../assets/badge.png';
import clock from '../../assets/clock.png';

// import 'react-accessible-accordion/dist/fancy-example.css';

export default function ExpCard(props) {
  return (
    <div onClick={props.handleClick} className={styles.ExpcardContainer}>
      <div className={styles.ExpcardImgContainer}>
        <img src={bgimg} alt='' className={styles.ExpcardImg} />
        <div className={styles.ExpRightItem}>UGC, 1/20 Attempts</div>
        <div>
          <img src={share} alt='' className={styles.ExpshareImg} />
        </div>
      </div>
      <div className={styles.ExpcardContentArea}>
        <div className={styles.ExpcardContentTitle}>Opinion with Badges</div>
        <div className={styles.ExpcardContentDesc}>Opinion with Badges</div>
        <div className={styles.ExpcardContentFlexHolder}>
          <div className={styles.ExpcardContentFlex}>
            <img src={clock} alt='' className={styles.ExpitemImg} />
            <div>Ends in 2 weeks</div>
          </div>
          <div className={styles.ExpcardContentFlex}>
            <img src={badge} alt='' className={styles.ExpitemImg} />
            <div>50% Off On all Products</div>
          </div>
        </div>
      </div>
    </div>
  );
}
